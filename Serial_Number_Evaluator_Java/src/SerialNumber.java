import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Creates an abstract serial number class
 *
 * Liam Nunes
 * CIS248 Final Project
 */

public abstract class SerialNumber {
    public SerialNumber(String sn) {

    }
    public SerialNumber() {

    }

    public abstract boolean isValid();


}

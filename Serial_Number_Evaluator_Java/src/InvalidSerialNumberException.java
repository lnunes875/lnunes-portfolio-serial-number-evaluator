//Liam Nunes
public class InvalidSerialNumberException extends Exception{

    public InvalidSerialNumberException() {}

    public InvalidSerialNumberException(String message)
    {
        super(message);
    }
}

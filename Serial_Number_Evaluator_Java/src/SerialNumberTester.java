/**
 * This program demonstrates the SerialNumber class.
 *
 * Liam Nunes
 * CIS248 Final Project
 */
public class SerialNumberTester {
    public static void main(String[] args){
        String serial1 = "GHTRJ-GGGG-AQWR"; // Valid
        String serial2 = "11111-8975-1111"; // Invalid
        String serial3 = "GHTRJ-8J75-AQWR"; // Invalid
        String serial4 = "GHTRJ-8975-AQ2R"; // Invalid

        // Validate each serial number.
        //testIt(new LettersOnlySerialNumber(serial1));
        //testIt(new LettersOnlySerialNumber(serial2));
        //testIt(new LettersOnlySerialNumber(serial3));
        //testIt(new LettersOnlySerialNumber(serial4));
    }

    /**
     * This method takes a SerialNumber object and reports
     * whether or not it is a valid serial number.
     * @param sn - a SerialNumber object referance
     */
    private static void testIt(LettersOnlySerialNumber sn) {
        String q  = (sn.isValid()) ? "" : "not ";
        System.out.println(sn.toString() + " is " + q + "valid");
    }
}

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * The DigitsOnlySerialNumber class takes a software serial number in
 * the form of DDDDD-DDDD-DDDD where each L is a letter and
 * each D is a digit. The serial number has three groups of
 * characters, separated by hyphens. The class extracts the
 * three groups of characters and validates them.
 *
 * Liam Nunes
 * CIS248 Final Project
 */

public class DigitsOnlySerialNumber extends SerialNumber{
    // Declare class member variables/fields here. Refer
    // to the UML diagram provided in the project PDF
    private String first, second, third;
    private boolean valid;
    private final int MAX_FIRST = 5;
    private final int MAX_SECOND = 4;
    private final int MAX_THIRD = 4;


    /**
     * The constructor breaks a serial number into three
     * groups and each group is validated.
     * @param sn A serial number.
     */
    public DigitsOnlySerialNumber(String sn) throws InvalidSerialNumberException {

        breakString(sn);

        validate();

    }

    /**
     * the breakString Method breaks the given string into first second and third
     */
    private void breakString(String str) throws InvalidSerialNumberException{
        StringTokenizer st = new StringTokenizer(str,"-") ;

        try {
            first = st.nextToken();
            second = st.nextToken();
            third = st.nextToken();

        } catch (NoSuchElementException exp) {

            throw new InvalidSerialNumberException("Serial Number Does Not Have Proper Number of -'s Unable To Test");
        }
    }


    /**
     * The isValid method returns a boolean value indicating
     * whether the serial number is valid.
     *
     * @return true if the serial number is valid, false if not.
     */
    public boolean isValid()
    {
        return valid;
    }


    /**
     * The validate method sets the valid field to true if the serial
     * number is valid. Otherwise it sets valid to false. HINT: this
     * method will be used in the constructor
     */
    private void validate() throws InvalidSerialNumberException {
        if (isFirstGroupValid() && isSecondGroupValid() && isThirdGroupValid()){
            valid = true;
        }else{
            valid = false;
            throw new InvalidSerialNumberException("Serial Number Given is not DDDDD-DDDD-DDDD");
        }
    }


    /**
     * The isFirstGroupValid method validates the first group of
     * characters.
     *
     * @return true if the group is valid, false if not.
     */
    private boolean isFirstGroupValid() {
        if(first.length() != MAX_FIRST){
            return false;
        }
        for(int i =0; i < first.length(); i++) {
            if(!Character.isDigit(first.charAt(i))){
                return false;
            }
        }
        return true;
    }


    /**
     * The isSecondGroupValid method validates the second group of
     * characters.
     *
     * @return true if the group is valid, false if not.
     */
    private boolean isSecondGroupValid() {
        if(second.length() != MAX_SECOND){
            return false;
        }
        for(int i =0; i < second.length(); i++) {
            if(!Character.isDigit(second.charAt(i))){
                return false;
            }
        }
        return true;
    }


    /**
     * The isThirdGroupValid method validates the third group of
     * characters.
     *
     * @return true if the group is valid, false if not.
     */
    private boolean isThirdGroupValid() {
        if(third.length() != MAX_THIRD){
            return false;
        }
        for(int i =0; i < third.length(); i++) {
            if(!Character.isDigit(third.charAt(i))){
                return false;
            }
        }
        return true;
    }

    /**
     * Represent the full serial as a string
     * @return String with full serial
     */
    public String toString(){
        return first + "-" + second + "-" + third;
    }

}

import java.util.*;

/**
 * This program demonstrates the SerialNumber classes.
 *
 * Liam Nunes
 * CIS248 Final Project
 */
public class SerialDriver {
    public static void main(String[] args){
        //user input serial numbers and add to different storage methods
        Scanner scnr = new Scanner(System.in);
        String[] serialNumberStorage = new String[8];
        System.out.println("Please enter 8 serial numbers to be tested and validated");
        System.out.println("Serial numbers will be compared to the following accepted forms:");
        System.out.println("Letters Only: LLLLL-LLLL-LLLL");
        System.out.println("Digits Only: DDDDD-DDDD-DDDD");
        System.out.println("Digits and Letters: LLLLL-DDDD-LLLL");
        System.out.println("Each entered serial will be placed in several data structures and each entry of each data");
        System.out.println("structure will be tested against the accepted forms of a serial number");
        for(int q = 0 ; q < 8 ; q++){
            System.out.print("Please input serial number " + (q + 1) + ": ");
            serialNumberStorage[q] = scnr.nextLine();
        }


        LinkedList<SerialNumber> serialLinkedList = new LinkedList<>();

        Stack<SerialNumber> serialNumberStack = new Stack();

        Queue<SerialNumber> serialNumbersQueue = new LinkedList<>();

        Hashtable serialNumberHash = new Hashtable();

        int letOnlyCount = 1;
        int digOnlyCount = 1;
        int letAndDigCount = 1;

        //tests all serial numbers in all storage units
        for( int i = 0 ; i < 4 ; i++ ){//storage method
            if( i == 0 ){System.out.println("Testing With Linked List:");}
            if( i == 1 ){System.out.println("Testing With Stack:");}
            if( i == 2 ){System.out.println("Testing With Queue:");}
            if( i == 3 ){System.out.println("Testing With Hash Table:");}
            for( int j = 0 ; j < 3 ; j++){//serial number type
                if( j == 0 ){System.out.println("   Testing Letters Only:");}
                if( j == 1 ){System.out.println("   Testing Digits Only:");}
                if( j == 2 ){System.out.println("   Testing Digits and Letters:");}
                for( int k = 0 ; k < serialNumberStorage.length ; k++ ){//serial number entered
                    if( i == 0 ){//Testing With Linked List
                        if(j == 0) {//letter only
                            try {
                                serialLinkedList.add(new LettersOnlySerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new LettersOnlySerialNumber(serialNumberStorage[k]) + " is valid with LettersOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with LettersOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 1) {//digit only
                            try {
                                serialLinkedList.add(new DigitsOnlySerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new DigitsOnlySerialNumber(serialNumberStorage[k]) + " is valid with DigitsOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 2) {//letter and digit
                            try {
                                serialLinkedList.add(new DigitsAndLettersSerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new DigitsAndLettersSerialNumber(serialNumberStorage[k]) + " is valid with DigitsAndLettersSerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsAndLettersSerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }

                    }
                    else if( i == 1){//testing with stack
                        if(j == 0) {//letter only
                            try {
                                serialNumberStack.push(new LettersOnlySerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new LettersOnlySerialNumber(serialNumberStorage[k]) + " is valid with LettersOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with LettersOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 1) {//digit only
                            try {
                                serialNumberStack.push(new DigitsOnlySerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new DigitsOnlySerialNumber(serialNumberStorage[k]) + " is valid with DigitsOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 2) {//letter and digit
                            try {
                                serialNumberStack.push(new DigitsAndLettersSerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new DigitsAndLettersSerialNumber(serialNumberStorage[k]) + " is valid with DigitsAndLettersSerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsAndLettersSerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                    }
                    else if( i == 2){//testing with queue
                        if(j == 0) {//letter only
                            try {
                                serialNumbersQueue.add(new LettersOnlySerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new LettersOnlySerialNumber(serialNumberStorage[k]) + " is valid with LettersOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with LettersOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 1) {//digit only
                            try {
                                serialNumbersQueue.add(new DigitsOnlySerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new DigitsOnlySerialNumber(serialNumberStorage[k]) + " is valid with DigitsOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 2) {//letter and digit
                            try {
                                serialNumbersQueue.add(new DigitsAndLettersSerialNumber(serialNumberStorage[k]));
                                System.out.println("        " + new DigitsAndLettersSerialNumber(serialNumberStorage[k]) + " is valid with DigitsAndLettersSerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsAndLettersSerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                    }
                    else{//testing hashtable
                        if(j == 0) {//letter only
                            try {
                                serialNumberHash.put(("LetterOnlySerialNumber" + letOnlyCount) , new LettersOnlySerialNumber(serialNumberStorage[k]));
                                letOnlyCount++;
                                System.out.println("        " + new LettersOnlySerialNumber(serialNumberStorage[k]) + " is valid with LettersOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with LettersOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 1) {//digit only
                            try {
                                serialNumberHash.put(("DigitOnlySerialNumber" + digOnlyCount) , new DigitsOnlySerialNumber(serialNumberStorage[k]));
                                digOnlyCount++;
                                System.out.println("        " + new DigitsOnlySerialNumber(serialNumberStorage[k]) + " is valid with DigitsOnlySerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsOnlySerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                        if(j == 2) {//letter and digit
                            try {
                                serialNumberHash.put(("LetterAndDigitsSerialNumber" + letAndDigCount) , new DigitsAndLettersSerialNumber(serialNumberStorage[k]));
                                letAndDigCount++;
                                System.out.println("        " + new DigitsAndLettersSerialNumber(serialNumberStorage[k]) + " is valid with DigitsAndLettersSerialNumber");
                            } catch (InvalidSerialNumberException ex) {
                                System.out.print("        " + serialNumberStorage[k] + " is invalid with DigitsAndLettersSerialNumber");
                                System.out.println(" Because: " + ex.getLocalizedMessage());
                            }
                        }
                    }
                }
            }
            System.out.println();
        }

        //print final serial list
        System.out.println("Contents of Serial List when only adding valid Serial numbers:");
        System.out.println(serialLinkedList);
        System.out.println();

        //print final stack
        System.out.println("Contents of Stack when only adding valid Serial numbers:");
        System.out.println(serialNumberStack);
        System.out.println();

        //print final queue
        System.out.println("Contents of Queue when only adding valid Serial numbers:");
        System.out.println(serialNumbersQueue);
        System.out.println();

        //print final Hashtable
        System.out.println("Contents of Hashtable when only adding valid Serial numbers:");
        System.out.println(serialNumberHash);
        System.out.println();


    }

}
